(require 'ids-find)
(require 'cwiki-common)

(defvar chise-ipfs-ipns-directory "~/ipns/chise/v1/character")

(defun chise-ipfs-encode-char (character)
  (cond
   ((encode-char character '=ucs)
    (char-to-string character)
    )
   (t
    (www-uri-encode-object character))))

;; (unless (file-exists-p chise-ipfs-ipns-directory)
;;   (make-directory chise-ipfs-ipns-directory t))

(let ((coding-system-for-write 'utf-8-mcs-er)
      key val rep grain base domain i path)
  (map-char-attribute
   (lambda (c v)
     (with-temp-buffer
       (insert
	(format "[%s]\n"
		(mapconcat
		 (lambda (c)
		   (format "%S"
			   (chise-ipfs-encode-char c)))
		 (sort (copy-list v) #'<) ",\n ")))
       (dolist (cell (char-attribute-alist c))
	 (setq key (car cell)
	       val (cdr cell))
	 (when (and (find-charset key)
		    val)
	   (if (eq key '=ucs)
	       (setq grain 'a
		     base 'ucs
		     domain nil)
	     (setq rep (www-uri-encode-feature-name key)
		   i 0)
	     (setq grain
		   (if (string-match "\\." rep)
		       (prog1
			   (intern (substring rep 0 (match-beginning 0)))
			 (setq i (match-end 0)))
		     'rep))
	     (if (string-match "@" rep i)
		 (setq base (intern (substring rep i (match-beginning 0)))
		       domain (intern (substring rep (match-end 0))))
	       (setq base (intern (substring rep i))
		     domain nil)))
	   (setq path
		 (expand-file-name
		  (format (if (char-feature-property base 'value-presentation-type)
			      "%d"
			    "0x%X")
			  val)
		  (expand-file-name (format "%s"
					    (or domain "default"))
				    (expand-file-name
				     (format "%s" base)
				     (expand-file-name
				      (format "%s" grain)
				      chise-ipfs-ipns-directory)))))
	   (unless (file-exists-p path)
	     (make-directory path t))
	   (write-region (point-min)(point-max)
			 (expand-file-name
			  "ideographic-products.json" path)))))
     nil)
   'ideographic-products))
