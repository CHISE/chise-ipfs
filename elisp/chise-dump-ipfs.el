(require 'ipld)
(require 'ids-find)
(require 'cwiki-common)

(defvar chise-ipfs-ipns-directory "~/ipns/chise/v3/character")

(defun char-ccs-pairs (character)
  (let (ccs-pairs)
    (dolist (cell (char-attribute-alist character))
      (when (and (find-charset (car cell))
		 (not (string-match "^=.*\\(gt-pj-\\|hanyo-denshi/../mf\\)"
				    (symbol-name (car cell)))))
	(setq ccs-pairs (cons cell ccs-pairs))))
    ccs-pairs))

(defun chise-parse-feature-name (feature-name)
  (let ((name (symbol-name feature-name))
	base domain meta number)
    (if (string-match "\\*" name)
	(setq meta (intern (substring name (match-end 0)))
	      base (substring name 0 (match-beginning 0)))
      (setq base name))
    (if (string-match "\\$_\\([0-9]+\\)$" base)
	(setq number (string-to-int (match-string 1 base))
	      base (substring base 0 (match-beginning 0))))
    (if (string-match "@" base)
	(setq domain (intern (substring base (match-end 0)))
	      base (intern (substring base 0 (match-beginning 0))))
      (setq base (if (string= base "")
		     nil
		   (intern base))))
    (list base domain meta number)))

    
(defvar chise-ipfs-ccs-priority-list
  '(=mj
    =adobe-japan1-0
    =adobe-japan1-1
    =adobe-japan1-2
    =adobe-japan1-3
    =adobe-japan1-4
    =adobe-japan1-5
    =adobe-japan1-6
    =daikanwa
    =ucs-itaiji-001
    =ucs-itaiji-002
    =ucs-itaiji-003
    =ucs-itaiji-004
    =ucs-itaiji-005
    =ucs-itaiji-006
    =ucs-itaiji-007
    =ucs-itaiji-008
    =ucs-itaiji-009
    =ucs-itaiji-010
    =ucs-itaiji-011
    =ucs-itaiji-012
    =ucs-itaiji-013
    =ucs-itaiji-014
    =ucs-itaiji-016
    =ucs-itaiji-018
    =ucs-itaiji-084
    =ucs-var-001
    =ucs-var-002
    =ucs-var-003
    =ucs-var-004
    =ucs-var-005
    =ucs-var-006
    =ucs-var-007
    =ucs-var-008
    =ucs-var-010
    =gt
    =daijiten
    =jis-x0208 =jis-x0208@1990
    =jis-x0213-2
    =jis-x0212
    =jis-x0213-1@2000 =jis-x0213-1@2004
    =jis-x0208@1983 =jis-x0208@1978
    =cns11643-1 =cns11643-2 =cns11643-3
    =cns11643-4 =cns11643-5 =cns11643-6 =cns11643-7
    =gb2312 =gb12345
    =big5-cdp
    =cbeta =jef-china3
    =gt-k
    =zinbun-oracle
    =ruimoku-v6))
    
(defun chise-ipfs-char-feature-name< (ka kb)
  (let ((pos_a (position ka chise-ipfs-ccs-priority-list))
	(pos_b (position kb chise-ipfs-ccs-priority-list)))
    (cond
     (pos_a
      (cond
       (pos_b
	(< pos_a pos_b)
	)
       (t))
      )
     (pos_b
      nil)
     (t
      (char-attribute-name< ka kb)))))

(defun chise-format-ccs-code-point (ccs code-point)
  (let (ccs-uri-name base i minimum-width)
    (setq ccs-uri-name (www-uri-encode-feature-name ccs)
	  i 0)
    (if (string-match "\\." ccs-uri-name)
	(setq i (match-end 0)))
    (if (string-match "@" ccs-uri-name i)
	(setq base (intern (substring ccs-uri-name i (match-beginning 0))))
      (setq base (intern (substring ccs-uri-name i))))
    (setq minimum-width (char-feature-property
			 base
			 'value-presentation-minimum-width))
    (format (format "%%s=%s"
		    (if (eq (char-feature-property base 'value-presentation-type)
			    'decimal)
			(if minimum-width
			    (format "%%0%dd" minimum-width)
			  "%d")
		      "0x%X"))
	    ccs-uri-name code-point)))

(defun chise-ipfs-encode-char (character)
  (let (spec ret ucs)
    (if (and (consp character)
	     (setq ret (find-char character)))
	(setq character ret))
    (cond
     ((and (characterp character)
	   (encode-char character '=ucs))
      (char-to-string character)
      )
     ((and (characterp character)
	   (setq spec (sort (char-ccs-pairs character)
			    (lambda (a b)
			      (chise-ipfs-char-feature-name< (car a)(car b))))))
      (chise-format-ccs-code-point (car (car spec))
				   (cdr (car spec)))
      )
     ((setq ret (if (characterp character)
		    (get-char-attribute character 'ideographic-combination)
		  (cdr (assq 'ideographic-combination character))))
      (concat "str_"
	      (mapconcat (lambda (cell)
			   (if (setq ucs (if (consp cell)
					     (cdr (assq '=ucs cell))
					   (encode-char cell '=ucs)))
			       (format "%d" ucs)
			     (chise-ipfs-encode-char cell)))
			 ret "," ))
      )
     ((setq ret (if (characterp character)
		    (get-char-attribute character 'ideographic-structure)
		  (cdr (assq 'ideographic-structure character))))
      (concat "ids_"
	      (mapconcat (lambda (cell)
			   (if (setq ucs (if (consp cell)
					     (cdr (assq '=ucs cell))
					   (encode-char cell '=ucs)))
			       (format "%d" ucs)
			     (chise-ipfs-encode-char cell)))
			 ret "," ))
      )
     (t
      (format "system-char-id=0x%X"
	      (encode-char character 'system-char-id))
      ))))

;; (defun chise-ipfs-ccs-object-path (ccs)
;;   (let ((ccs-rep (www-uri-encode-feature-name ccs))
;;         (i 0)
;;         ccs-grain ccs-base ccs-domain)
;;     (setq ccs-grain (if (string-match "\\." ccs-rep)
;;                         (prog1
;;                             (intern (substring ccs-rep 0 (match-beginning 0)))
;;                           (setq i (match-end 0)))
;;                       'rep))
;;     (if (string-match "@" ccs-rep i)
;;         (setq ccs-base (intern (substring ccs-rep i (match-beginning 0)))
;;               ccs-domain (intern (substring ccs-rep (match-end 0))))
;;       (setq ccs-base (intern (substring ccs-rep i))
;;             ccs-domain nil))
;;     (cons
;;      (expand-file-name
;;       (format "%s"
;;               (or ccs-domain "default"))
;;       (expand-file-name (format "%s" ccs-base)
;;                         (expand-file-name (format "%s" ccs-grain)
;;                                           chise-ipfs-ipns-directory)))
;;      (if (char-feature-property ccs-base 'value-presentation-type)
;;          "%d.json"
;;        "0x%X.json"))))
		
(defun chise-ipfs-char-object-path (character)
  (let (ucs rep ccs-rep cpos-rep grain base domain i)
    (if (setq ucs (encode-char character '=ucs))
	(format "a/ucs/default/0x%x/%02x/%02x"
		(lsh ucs -16)
		(logand (lsh ucs -8) 255)
		(logand ucs 255))
      (setq rep (chise-ipfs-encode-char character))
      (cond
       ((string-match "^\\(str\\|ids\\)_" rep)
	(concat (match-string 1 rep) "/" (substring rep (match-end 0)))
	)
       ((string-match "=0x" rep)
	(setq ccs-rep (substring rep 0 (match-beginning 0))
	      cpos-rep (string-to-int (substring rep (match-end 0)) 16)
	      i 0)
	(setq grain
	      (if (string-match "\\." ccs-rep)
		  (prog1
		      (intern (substring ccs-rep 0 (match-beginning 0)))
		    (setq i (match-end 0)))
		'rep))
	(if (string-match "@" ccs-rep i)
	    (setq base (intern (substring ccs-rep i (match-beginning 0)))
		  domain (intern (substring ccs-rep (match-end 0))))
	  (setq base (intern (substring ccs-rep i))
		domain nil))
	(format "%s/%s/%s/0x%x/%02x/%02x"
		grain
		base
		(or domain 'default)
		(lsh cpos-rep -16)
		(logand (lsh cpos-rep -8) 255)
		(logand cpos-rep 255))
	)
       ((string-match "=" rep)
	(setq ccs-rep (substring rep 0 (match-beginning 0))
	      cpos-rep (string-to-int (substring rep (match-end 0)))
	      i 0)
	(setq grain
	      (if (string-match "\\." ccs-rep)
		  (prog1
		      (intern (substring ccs-rep 0 (match-beginning 0)))
		    (setq i (match-end 0)))
		'rep))
	(if (string-match "@" ccs-rep i)
	    (setq base (intern (substring ccs-rep i (match-beginning 0)))
		  domain (intern (substring ccs-rep (match-end 0))))
	  (setq base (intern (substring ccs-rep i))
		domain nil))
	(format "%s/%s/%s/0x%x/%02x/%02x"
		grain
		base
		(or domain 'default)
		(lsh cpos-rep -16)
		(logand (lsh cpos-rep -8) 255)
		(logand cpos-rep 255))
	)
       (t rep)
       ))))

      
;; (unless (file-exists-p chise-ipfs-ipns-directory)
;;   (make-directory chise-ipfs-ipns-directory t))

(defun chise-ipfs-format-character (value &optional level)
  (unless level
    (setq level 0))
  (let (ret indent indent-3)
    (if (and (consp value)
	     (setq ret (find-char value)))
	(setq value ret))
    (cond ((characterp value)
	   (format "%S"
		   (if (encode-char value '=ucs)
		       (char-to-string value)
		     (chise-ipfs-encode-char value)))
	   )
	  ((consp value)
	   (cond
	    ((setq ret (assq 'ideographic-structure value))
	     (setq indent-3
		   (if (>= level 1)
		       (make-string (1- level) ?\ )
		     ""))
	     (setq indent (make-string (+ level 2) ?\ ))
	     (format "{\"ideographic-structure\":[\n%s%s]\n%s}"
		     indent
		     (mapconcat (lambda (c)
				  (chise-ipfs-format-character c (+ level 3)))
				(cdr ret)
				(concat ",\n" indent))
		     indent-3)
	     )
	    ((setq ret (assq 'ideographic-combination value))
	     (setq indent-3
		   (if (>= level 1)
		       (make-string (1- level) ?\ )
		     ""))
	     (setq indent (make-string (+ level 2) ?\ ))
	     (format "{\"ideographic-combination\":[\n%s%s]\n%s}"
		     indent
		     (mapconcat (lambda (c)
				  (chise-ipfs-format-character c (+ level 3)))
				(cdr ret)
				(concat ",\n" indent))
		     indent-3)
	     ))
	   ))))

(defun chise-ipfs-format-object (value &optional level)
  (unless level
    (setq level 0))
  (cond
   ((characterp value)
    (chise-ipfs-format-character value (+ level 3))
    )
   ((concord-object-p value)
    (cond
     ((< level 4)
      (let ((genre (concord-object-genre value))
	    (spec (concord-object-spec value))
	    indent indent-3)
	(setq indent-3
	      (if (>= level 1)
		  (make-string (1- level) ?\ )
		""))
	(setq indent (make-string (+ level 2) ?\ ))
	(format "{\"@type\":\"concord:%s\",\n%s%s\n%s}"
		genre
		indent
		(mapconcat (lambda (cell)
			     (format "\"%s\":%s"
				     (car cell)
				     (chise-ipfs-format-value
				      (car cell)
				      (cdr cell)
				      (+ level 1))))
			   spec
			   (concat ",\n" indent))
		indent-3))
      )
     (t
      (format "{\"@type\":\"concord:%s\",\"@id\":\"concord:%s\"%s}"
	      (concord-object-genre value)
	      (concord-object-id value)
	      (let ((name (or (concord-object-get value '=name)
			      (concord-object-get value 'name))))
		(if name
		    (format ",\"name\":\"%s\"" name)
		  "")))
      ))
    )
   (t
    (format "%S" value)
    )))

(defun chise-ipfs-format-character-list (value &optional level)
  (unless level
    (setq level 0))
  (cond
   ((consp value)
    (format "[%s]"
	    (mapconcat (lambda (c)
			 (chise-ipfs-format-character c (+ 2 level)))
		       value ",\n "))
    )
   ((characterp value)
    (chise-ipfs-format-character value (+ 2 level))
    )
   (t
    (format "%S" value)
    )))

(defun chise-ipfs-format-sorted-character-list (value &optional level predicate)
  (unless level
    (setq level 0))
  (unless predicate
    (setq predicate #'<))
  (cond
   ((consp value)
    (format "[%s]"
	    (mapconcat (lambda (c)
			 (chise-ipfs-format-character c (+ 2 level)))
		       (sort (copy-list value) predicate)
		       ",\n "))
    )
   ((characterp value)
    (chise-ipfs-format-character value (+ 2 level))
    )
   (t
    (format "%S" value)
    )))

(defun chise-ipfs-format-object-list (value &optional level)
  (unless level
    (setq level 0))
  (cond
   ((consp value)
    (format "[%s]"
	    (mapconcat (lambda (c)
			 (chise-ipfs-format-object c (+ 2 level)))
		       value ",\n "))
    )
   ((characterp value)
    (chise-ipfs-format-character value (+ 2 level))
    )
   (t
    (format "%S" value)
    )))

(defun chise-ipfs-format-value-symbol-list (value)
  (format "[%s]\n"
	  (mapconcat
	   (lambda (symbol)
	     (format "\"%s\"" symbol))
	   value ",\n ")))

(defun chise-ipfs-format-link (value)
  (if (and (consp value)
	   (eq (car value) 'link))
      (let ((props (nth 1 value))
	    (desc (nth 2 value))
	    url rest)
	(setq url (plist-get props :ref))
	(setq rest props
	      props nil)
	(while rest
	  (if (eq (car rest) :ref)
	      (setq rest (cdr (cdr rest)))
	    (setq props (cons (cons (intern (substring (symbol-name (pop rest)) 1))
				    (pop rest))
			      props))))
	(if desc
	    (setq props (cons (cons 'description desc)
			      props)))
	(if url
	    (setq props (cons (cons 'url url)
			      props)))
	(format "{\n%s\n}"
		(mapconcat
		 (lambda (cell)
		   (format "\"%s\":%S" (car cell)(cdr cell)))
		 props ",\n")))
    (format "%S" value)))

(defun chise-ipfs-format-links (value)
  (format "[\n%s\n]"
	  (mapconcat
	   #'chise-ipfs-format-link
	   value  ",\n")))

(defun chise-ipfs-format-composition (value)
  (format "{\n %s\n}"
	  (mapconcat
	   (lambda (cell)
	     (format "\"%c\":\"%c\""
		     (car cell)(cdr cell)))
	   value ",\n ")))

(defun chise-ipfs-format-tag (value &optional level)
  (unless level
    (setq level 0))
  (cond
   ((consp value)
    (let ((tag (car value))
	  (props (nth 1 value))
	  (body (nthcdr 2 value))
	  indent indent-1 key)
      (setq indent-1 (make-string (1+ level) ?\ ))
      (setq indent (make-string (+ level 2) ?\ ))
      (format "{%s\n%s\"description\":[\n%s%s\n%s]}"
	      (mapconcat (lambda (cell)
			   (format "\"%s\":%s"
				   (car cell)
				   (chise-ipfs-format-tag (cdr cell)
							  (+ level 3))))
			 (cons (cons 'XMLtag tag)
			       (mapcar (lambda (cell)
					 (setq key (symbol-name (car cell)))
					 (if (string-match "^:" key)
					     (cons (intern (substring key 1))
						   (cdr cell))
					   cell))
				       (plist-to-alist props)))
			 (concat ",\n" indent-1))
	      indent-1
	      indent
	      (mapconcat (lambda (element)
			   (chise-ipfs-format-tag element (+ level 3)))
			 body
			 (concat ",\n" indent))
	      indent-1))
    )
   ((concord-object-p value)
    (chise-ipfs-format-object value (1+ level))
    )
   ((stringp value)
    (format "%S" value)
    )
   ((characterp value)
    (chise-ipfs-format-character value (1+ level))
    )
   (t
    (json-encode value)
    )))

(defun chise-ipfs-format-tag-list (value &optional level)
  (unless level
    (setq level 0))
  (cond
   ((consp value)
    (format "[%s]"
	    (mapconcat (lambda (c)
			 (chise-ipfs-format-tag c (+ 2 level)))
		       value ",\n "))
    )
   ((eq value nil)
    ""
    )
   (t
    (format "%S" value)
    )))

(defun chise-ipfs-format-value (feature-name value &optional level)
  (cond
   ((find-charset feature-name)
    (if value
	(number-to-string value)
      "null")
    )
   ((memq feature-name '(=id =fanqie))
    (format "\"%s\"" value)
    )
   ((eq feature-name 'name)
    (format "%S" value)
    )
   ((eq feature-name 'sources)
    (chise-ipfs-format-value-symbol-list value)
    )
   ((memq feature-name '(references note))
    (chise-ipfs-format-links value)
    )
   ((memq feature-name '(instance 
			 *instance
			 abstract-glyph sound))
    (chise-ipfs-format-object-list value level)
    )
   ((memq feature-name '(<-denotational <-subsumptive))
    (chise-ipfs-format-character-list value level)
    )
   ((string-match "^\\(<-\\|->\\)" (symbol-name feature-name))
    (chise-ipfs-format-object-list value level)
    )
   ((eq feature-name 'ideographic-products)
    (chise-ipfs-format-sorted-character-list value level)
    )
   ((memq feature-name '(ideographic-structure
			 ideographic-combination
			 =decomposition
			 =>decomposition
			 synonymous-character
			 character
			 characters))
    (chise-ipfs-format-character-list value level)
    )
   ((eq feature-name 'composition)
    (chise-ipfs-format-composition value)
    )
   ((eq feature-name 'hdic-syp-description)
    (chise-ipfs-format-tag-list value level)
    )
   (t
    (json-encode value)
    )))

(defun chise-ipfs-dump-feature (feature-name)
  (let ((coding-system-for-write 'utf-8-mcs-er)
	ccs-rep i ccs-path
	ccs-grain ccs-base ccs-domain
        ;; ccs-format
	base domain meta val-index
	ret rep path file-name)
    (when (find-charset feature-name)
      (setq ccs-rep (www-uri-encode-feature-name feature-name)
	    i 0)
      (setq ccs-grain (if (string-match "\\." ccs-rep)
			  (prog1
			      (intern (substring ccs-rep 0 (match-beginning 0)))
			    (setq i (match-end 0)))
			'rep))
      (if (string-match "@" ccs-rep i)
	  (setq ccs-base (intern (substring ccs-rep i (match-beginning 0)))
		ccs-domain (intern (substring ccs-rep (match-end 0))))
	(setq ccs-base (intern (substring ccs-rep i))
	      ccs-domain nil))
      (setq ccs-path (expand-file-name
		      (format "%s"
			      (or ccs-domain "default"))
		      (expand-file-name
		       (format "%s" ccs-base)
		       (expand-file-name
			(format "%s" ccs-grain)
			(expand-file-name "index"
					  chise-ipfs-ipns-directory)))))
      ;; (setq ccs-format (if (char-feature-property ccs-base 'value-presentation-type)
      ;;                      "%d.json"
      ;;                    "0x%X.json"))
      (unless (file-exists-p ccs-path)
	(make-directory ccs-path t)))
    (setq ret (chise-parse-feature-name feature-name))
    (setq base (pop ret)
	  domain (pop ret)
	  meta (pop ret)
	  val-index (pop ret))
    (unless base
      (if meta
	  (if (setq ret (chise-parse-feature-name meta))
	      (setq base (intern (format "*%s" (pop ret)))
		    domain (pop ret)
		    meta (pop ret)
		    val-index (pop ret))
	    (setq base (intern (format "*%s" meta))
		  meta nil))))
    (map-char-attribute
     (lambda (c v)
       (with-temp-buffer
	 (insert
	  (chise-ipfs-format-value (or meta base) v))
         ;; (setq rep (chise-ipfs-encode-char c))
         ;; (if (= (length rep) 1)
         ;;     (setq rep (format "%d" (aref rep 0))))
	 (setq rep (chise-ipfs-char-object-path c))
	 (setq path
	       (expand-file-name
		(www-uri-encode-feature-name base)
		(expand-file-name
		 rep
		 (expand-file-name
		  "Object-ID" chise-ipfs-ipns-directory))))
	 (if meta
	     (setq path (expand-file-name
			 (if domain
			     (www-uri-encode-feature-name domain)
			   "default")
			 path)
		   file-name (if val-index
				 (format "%s.%d.json"
					 (www-uri-encode-feature-name meta)
					 val-index)
			       (format "%s.json"
				       (www-uri-encode-feature-name meta))))
	   (setq file-name
		 (format "%s.json"
			 (if domain
			     (www-uri-encode-feature-name domain)
			   "default"))))
	 (unless (file-exists-p path)
	   (make-directory path t))
	 (write-region (point-min)(point-max)
		       (expand-file-name
			file-name path))
	 (when (and ccs-path v)
	   (erase-buffer)
	   (insert rep)
	   (write-region (point-min)(point-max)
			 (expand-file-name (format "%d.json" v)
					   ccs-path)))
	 )
       nil)
     feature-name)))

(defun chise-ipfs-dump-all-features ()
  (dolist (feature-name (sort (copy-list (char-attribute-list))
			      #'char-attribute-name<))
    (princ (format "\nDumping %s..." feature-name))
    (chise-ipfs-dump-feature feature-name)
    (princ "...done.\n")
    ))
