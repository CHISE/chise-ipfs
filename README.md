# CHISE IPFS
A utilities for the CHISE character ontology on IPFS.

## [IPFS-CHISE IDS-find](https://www.chise.org/ipns/k51qzi5uqu5dkeviwyznbk4skcwm7596f4bl09ktf1r5z0orv66x6cnoa05gbr/index.ja.html)

A IPFS based Web interface to search Chinese characters including
specified components, like [CHISE
IDS-Find](https://www.chise.org/ids-find).
