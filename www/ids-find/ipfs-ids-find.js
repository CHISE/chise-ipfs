function EsTsplitObjectURLrep (string) {
    let ret = string.search(/[=:]/);

    if ( ret < 0 ){
	return [];
    }
    else {
	let fname = string.slice(0, ret);
	let value = string.slice(ret + 1);
	let grain = '';
	let base_with_domain = fname;

	ret = fname.search(/\./);	
	if ( ret >= 0 ) {
	    grain = fname.slice(0, ret);
	    base_with_domain = fname.slice(ret + 1);
	}

	let base = base_with_domain;
	let domain = '';
	ret = base_with_domain.search(/@/);
	if ( ret >= 0 ) {
	    base = base_with_domain.slice(0, ret);
	    domain = base_with_domain.slice(ret + 1);
	}
	return [ grain, base, domain, value ];
    }
}

async function getHanziProducts (character) {
  //const ipfs_gw_URL = "http://localhost:8080";
  const ipfs_gw_URL = "https://www.chise.org";
  //const ipfs_gw_URL = "https://ipfs.io";
  const requestURL = ipfs_gw_URL
    + "/ipfs/Qmf6Kwi8QqwsQRHMAHbZoq1LuPSZqD1XwNXHTx4HpqCbHG/v1/character/a/ucs/default/0x"
    + character.codePointAt(0).toString(16).toUpperCase() + "/ideographic-products.json";
  //console.log(requestURL);
  const request = new Request(requestURL);

  const response = await fetch(request);
  const hanzi_products = await response.json();

  return hanzi_products;
}

// getHanziProducts ('字').then((v) => { console.log (v); });


function intersection (sets) {
  let set0 = sets[0];

  for (const set of sets) {
    set0 = set0.filter((x) => set.includes(x));
  }
  return set0;
}

// p1 = getHanziProducts ('木');
// p2 = getHanziProducts ('口');
// Promise.all([p1, p2])
//   .then(([a, b]) => {
// 	  console.log (a.filter((val_a) => b.includes(val_a)));
// 	});


async function ipfsIDSfind (queries) {
  //const queries = ['一', '口', '土', '田'];

  // 並行に非同期処理を実効
  const ret = await Promise.all(queries.map(getHanziProducts));
  return intersection (ret);
}
